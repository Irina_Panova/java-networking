import com.client.Client;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author Irina Panova
 */
public class ClientTest {
     
    @Test
    public void getFirstMessageTest() {
        Client x = new Client();
        x.createGUI();
        
        Assert.assertEquals("Enter your login name", x.getMessage());
    }
}
