package com.client;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Java Client App which connects to Server via Soket
 * 
 * @author Irina Panova
 */
public class Client {

    // Path to configuration file
    private static final String PROPERTIES_PATH = "src/main/resources/config.properties";
    
    private static Properties properties;
    private static Socket socket; // Socket provides I/O channels to communicate between Client and Server
    private static BufferedReader reader; // Socket read stream
    private static PrintWriter writer; // Socket write stream  
    private static JTextArea incoming; // Area for incoming messages from Server
    private static JTextField outcoming; // Field for outcoming messages to Server
    private String message;

    public String getMessage() {
        return message;
    }
    
    /**
     * Get properties (address, port number) from file and set connection with the Server
     * 
     * @throws IOException
     * @throws FileNotFoundException 
     */
    private void openConnection() throws IOException, FileNotFoundException {
        properties = new Properties(); // Create a Properties object 
        properties.load(new FileReader(PROPERTIES_PATH)); // Load data from a file into it
        String host = properties.getProperty("server.host");
        int port = Integer.parseInt(properties.getProperty("server.port")); 
        
        socket = new Socket(host, port); // Create Socket and connect with the Server
        System.out.println("Client started...");
    }
    
    /**
     * Set incoming and outcoming Socket streams for messaging with the Server
     */
    private void setUpNetworking() {
        try {
            openConnection();
 
            // Bind BufferedReader with low-level InputStreamReader, which is already connected to the incoming stream of the Socket
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
            // Bind PrintWriter with low-level outcoming stream of the Socket
            writer = new PrintWriter(socket.getOutputStream());
            
            System.out.println("The connection with Server is established."); 
            
        } catch (IOException ex) {
            ex.printStackTrace();
            incoming.append("Connection with Server refused!!!");
        }
    }
     
    /**
     * Create Client GUI form
     */
    public void createGUI() {
        JFrame frame = new JFrame("Client");
        JPanel mainPanel = new JPanel();
        
        incoming = new JTextArea(15,55);
        incoming.setLineWrap(true);
        incoming.setWrapStyleWord(true);
        incoming.setEditable(false);
        JScrollPane qScroller = new JScrollPane(incoming);
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        outcoming = new JTextField(50);
        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(new SendButtonListener());
               
        mainPanel.add(qScroller);
        mainPanel.add(outcoming);
        mainPanel.add(sendButton);
        
        setUpNetworking();
        
        // Run thread, which is read data from the Server through a Socket
        // and output any incoming messages to the scrollable text area
        Thread readerThread = new Thread(new IncomingReader());
        readerThread.start();
        
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.pack();
        frame.setSize(700,600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);      
    }
    
    /**
     * Launch the application
     * 
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.createGUI();
    }
    
    /**
     * This class using when Client clicks button "Send", and text content send to the Server by writer variable
     */
    public class SendButtonListener implements ActionListener {
      
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {              
                writer.println(outcoming.getText()); // Data transmitted over the network to the Server
                writer.flush(); // Pull everything out of the buffer
                
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            
            outcoming.setText("");
            outcoming.requestFocus();
        }
    }
    
    /**
     * This class using when Client receives messages from the Server
     */
    public class IncomingReader implements Runnable {
        
        @Override
        public void run() {
            try {
                while((message = reader.readLine()) != null) {
                    incoming.append(message + "\n"); // Output message from the Server to the scrollable text area
                }

            } catch (IOException ex){
                ex.printStackTrace();
            }
        } 
    }
    
}
