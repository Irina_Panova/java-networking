# Game-Items-Store-App

## Overview
This project is a Client-Server implementation using Java Sockets.

### Client (GUI) App
1. The Client connects to the Server, by establishing connection with the Socket.
2. After a successful connection to the Server, the Client is prompted for enter a login name.
3. If a such Сlient is in the database, and he is not in the current session of the Server, the Client will be prompted to select one of the commands.
4. The Client sends a message to the Server with the selected command.

### Server (Console) App
1. The Server creates ServerSocket and waits for Client connection (Server can handle multiple Clients at the same time).
2. If the Client is connected, the Server starts to process its connection. Server forms a separate compute thread, in which the Client Socket is sent.
3. Server accepts and supports the following commands:

   - Login <player_name> - log in to the Server.
   - Logout - exit from the Game (close Socket).
   - Myinfo - prints the name, amount of money, and list of items purchased.
   - Viewshop - prints a list of items in the store with its price.
   - Buy <item_name> - buy an item.
   - Sell <item_name> - sell an item.

## Tools
- Maven
- MySQL

```
	<dependency>
       <groupId>mysql</groupId>
       <artifactId>mysql-connector-java</artifactId>
       <version>8.0.16</version>
   </dependency>
```

- JDBC
- JUnit

```
	<dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13-beta-3</version>
        <scope>test</scope>
    </dependency>
```

### Launch
java -jar Client-1.0-SNAPSHOT-jar-with-dependencies.jar

java -jar Server-1.0-SNAPSHOT-jar-with-dependencies.jar