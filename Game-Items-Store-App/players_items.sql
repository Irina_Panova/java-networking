CREATE TABLE `store`.`players_items`(
  `player_name` varchar(50) NOT NULL,
  `item_name` varchar(45) NOT NULL,
  PRIMARY KEY (`player_name`),
  FOREIGN KEY (`player_name`) REFERENCES players(`name`)
  ON DELETE CASCADE
  ON UPDATE CASCADE);