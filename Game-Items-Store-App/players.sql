CREATE TABLE `store`.`players` (
  `name` VARCHAR(50) NOT NULL,
  `money` DECIMAL(10,1) NOT NULL,
  PRIMARY KEY (`name`));