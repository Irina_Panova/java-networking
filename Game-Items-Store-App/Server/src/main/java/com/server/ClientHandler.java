package com.server;

import com.model.Item;

import java.io.*;
import java.net.*;
import java.util.*;
import java.sql.*;
import javax.xml.parsers.*;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;

/**
 * Сlient connection handler class
 * 
 * @author Irina Panova
 */
public class ClientHandler implements Runnable {
    
    // Path to configuration file
    private static final String PROPERTIES_PATH = "src/main/resources/connection.properties"; 
    
    private Socket socket;
    private ArrayList<String> logedClients; // List of loged Clients
    private Map<String, Item> items; // Items from XML file
    private static ArrayList<String> clients; // List of players (Clients) in database
    private static Set<String> playerItems; // Items which belong to player (Client)
    
    private static Properties properties;
    private static PrintWriter writer; // Socket write stream
    private static BufferedReader reader; // Socket read stream
    
    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    
    private String loginName;
    private BigDecimal money;

    public ClientHandler(Socket clientSocket, ArrayList<String> logedClients, Map<String, Item> items) {
        try {     
            this.socket = clientSocket;
            this.logedClients = logedClients;
            this.items = items;
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream());
            clients = new ArrayList<>();
            
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * @return CommandTypeEnum values
     */
    private CommandTypeEnum[] getListOfCommands() {   
        return CommandTypeEnum.values();
    }
    
    /**
     * Open connection to database (MySQL server)
     * 
     * @param query
     * @param preparedStmnt
     */
    private void openConnectionToDB(String query, boolean preparedStmnt) throws FileNotFoundException, IOException {
        properties = new Properties(); // Create a Properties object 
        properties.load(new FileReader(PROPERTIES_PATH)); // Load data from a file into it
        String url = properties.getProperty("URL"); 
        String username = properties.getProperty("Uname"); 
        String password = properties.getProperty("password");
        
        try {
            con = DriverManager.getConnection(url, username, password);
            
            if(!preparedStmnt) {
                // Getting Statement object to execute query
                stmt = con.createStatement();
                // Executing SELECT query
                rs = stmt.executeQuery(query);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Close connection to database (MySQL server)
     */
    private void closeConnectionToDB() {
        try {
            rs.close();
            stmt.close(); 
            con.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }           
    }
    
    /**
     * Load players from database
     */
    private void loadPlayersFromDB() throws IOException {
        String query = "SELECT name FROM players";

        try {
            openConnectionToDB(query, false);

            while (rs.next()) {
                clients.add(rs.getString("name")); 
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();   
        } finally {
            closeConnectionToDB();  
        }
    }
                    
    /**
     * Login Client on the Server
     */
    private void login() throws IOException {
        loadPlayersFromDB();
         
        writer.println("Enter your login name");
        writer.flush(); 
        
        while(true) {
            try {
                loginName = reader.readLine();
                
                // If a such Сlient is in the database and he is not in the current session of the Server
                if((clients.contains(loginName)) && (!logedClients.contains(loginName))) {
                    writer.println("You are logged in as '" + loginName + "'");
                    
                    logedClients.add(loginName);
                    System.out.println ("Loged Clients: " + logedClients.toString());
                    
                    writer.println("Enter one of these commands: " + Arrays.toString(getListOfCommands()));
                    writer.flush();
                    break;
                }
                else {
                    writer.println("You don't have permissions");
                    writer.flush();
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }         
        }
    }
    
    /**
     * Logout Client from the Server
     */
    private void logout() throws IOException {
        logedClients.remove(loginName);
        System.out.println ("Loged Clients: " + logedClients.toString());
        
        writer.println("'" + loginName + "'" + " is already loged out");
        writer.flush();
       
        socket.close();
        reader.close();
        writer.close();
    }
    
    /**
     * View all exist items in the shop
     */
    private void viewShop() {
        items.values().forEach((item) -> { 
            writer.println(item.toString());
        });
        writer.flush();
    }
    
    /**
     * Query Client's money from database
     */
    private BigDecimal queryMoney() throws IOException {
        String query = null;
        
        try {
            openConnectionToDB(query, true);
            
            PreparedStatement statement = con.prepareStatement("SELECT money FROM players WHERE name = ?");
            statement.setString(1, loginName); 
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
               money = rs.getBigDecimal(1);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnectionToDB();
        }    
        
        return money;   
    }
    
    /**
     * Query Client's items from database
     */
    private Set<String> queryItems() throws IOException {
        String query = null;
        playerItems = new HashSet<>();
        try {
            openConnectionToDB(query, true);
            
            PreparedStatement statement = con.prepareStatement("SELECT item_name FROM players_items WHERE player_name = ?");
            statement.setString(1, loginName); 
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
               playerItems.add(rs.getString(1));   
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnectionToDB();
        }        
       
        return playerItems;  
    }

    /**
     * Show Client's information
     */
    private void myInfo(){
        try {
            String query = null;
            openConnectionToDB(query, false);
            
            queryMoney();
            queryItems();
            
            writer.println("Client " + loginName + " has " + money);
            writer.println("Items:");
            playerItems.forEach((itemName) -> {
                writer.println(itemName);
            });
            writer.flush();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            closeConnectionToDB();
        }    
    }

    /**
     * Update information about bought in database 
     * 
     * @param item
     */
    private void updateBought(Item item) throws IOException { 
        String query = null;
        
        try {
            openConnectionToDB(query, true);
            
            PreparedStatement insertItem = con.prepareStatement("INSERT INTO players_items(player_name, item_name) VALUES (?, ?)");
            insertItem.setString(1, loginName);
            insertItem.setString(2, item.getName());
            
            PreparedStatement updateMoney = con.prepareStatement("UPDATE players SET money = ? WHERE name = ?");
            updateMoney.setBigDecimal(1, money.subtract(item.getCost()));
            updateMoney.setString(2, loginName);
            
            insertItem.executeUpdate();
            updateMoney.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
    
        } finally {
            closeConnectionToDB();
        }
    }

    /**
     * Make transaction -> buy an item
     */
    private void buy() {
        String message;
        
        writer.println("Enter item which you want to buy");
        writer.flush(); 
             
        while(true) {
            try {
                message = reader.readLine();
                
                Item item = items.get(message);
 
                if(items.containsValue(item)) {
                    queryMoney();
                
                    if(money.compareTo(item.getCost()) == -1) {
                        writer.println("You have not enough money to buy the " + item.getName());
                        writer.flush();
                        break;
                    }
                    else { 
                        writer.println("You have bought " + item.getName());
                        writer.flush();
                        updateBought(item); // update information in database
                        break;
                    }
                }
                else {
                    writer.println("No such item");
                    writer.flush();
                }
                
            } catch (IOException ex) {
                ex.printStackTrace();
            }             
        }
    }
    
    /**
     * Update information about sold in database 
     * 
     * @param item
     */    
    private void updateSold(Item item) throws IOException { 
        String query = null;
        
        try {
            openConnectionToDB(query, true);
            
            PreparedStatement deleteItem = con.prepareStatement("DELETE FROM players_items WHERE player_name = ? and item_name = ?");
            deleteItem.setString(1, loginName);
            deleteItem.setString(2, item.getName());
            
            PreparedStatement updateMoney = con.prepareStatement("UPDATE players SET money = ? WHERE name = ?");
            updateMoney.setBigDecimal(1, money.add(item.getCost()));
            updateMoney.setString(2, loginName);
            
            deleteItem.executeUpdate();
            updateMoney.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            
        } finally {
            closeConnectionToDB();
        }
    }

    /**
     * Make transaction -> sell an item
     */
    private void sell(){
        String message;
        
        writer.println("Enter item which you want to sell");
        writer.flush();  
        
        while(true) {
            try {
                message = reader.readLine();
                
                Item item = items.get(message);
                
                if(items.containsValue(item)) {
                    queryItems();

                    if(!playerItems.contains(item.getName())) {
                        writer.println("The item '" + item.getName() + "' donesn't exist");
                        writer.flush();
                        break;
                    }
                    else {                       
                        writer.println("You have sold " + item.getName());
                        writer.flush();
                        updateSold(item); // update information in database
                        break;
                    }
                }
                else {
                   writer.println("No such item");
                   writer.flush();
                }
                
            } catch (IOException ex) {
                ex.printStackTrace();
            }             
        }
    }
    
    @Override
    public void run() {
        String message;
        
        try {
            login();
            
            while(true) {
                message = reader.readLine();
                
                switch(message) {
                    case "LOGOUT":
                       logout();
                       break;
                    case "VIEWSHOP":
                        viewShop();
                        break;    
                    case "MYINFO":
                        myInfo();
                        break;
                    case "BUY":
                        buy();
                        break;
                    case "SELL":
                        sell();
                        break;  
                    default:
                        writer.println("No such command");
                        writer.flush();
                }
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
