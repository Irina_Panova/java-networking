package com.server;

public enum CommandTypeEnum { 
    LOGOUT,
    VIEWSHOP,
    MYINFO,
    BUY,
    SELL
}
