package com.server;

import com.model.Item;
import java.io.*;
import java.net.*;
import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.math.BigDecimal;
import org.joda.time.DateTime;
import org.xml.sax.SAXException;

/**
 * Java Server App which creates a ServerSocket and waits for Client requests
 * 
 * @author Irina Panova
 */
public class Server {

    // Path to configuration file
    private static final String PROPERTIES_PATH = "src/main/resources/config.properties"; 
    
    private static Properties properties;
    private static ServerSocket serverSocket; // ServerSocket opens the port and waits for Client connections
    private static Socket clientSocket;
    private static ArrayList<String> logedClients; // List of loged Clients
    private Map<String, Item> items; // Items from XML file
    
    public Server() {
        logedClients  = new ArrayList<>();
        items = new HashMap<>();
    }
           
    /**
     * Get properties (port number) from file and set connection with the Client
     * 
     * @throws IOException
     * @throws FileNotFoundException 
     */
    private void openConnection() throws IOException, FileNotFoundException {
        properties = new Properties(); // Create a Properties object 
        properties.load(new FileReader(PROPERTIES_PATH)); // Load data from a file into it
        int port = Integer.parseInt(properties.getProperty("server.port")); 

        serverSocket = new ServerSocket(port); // Create ServerSocket and waits for Client requests
        System.out.println("Server started...");
    }
    
    /**
     * Load items from XML file
     * 
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public Map<String, Item> loadItems() throws ParserConfigurationException, SAXException, IOException {
        File itemsFile = new File("src/main/resources/items.xml");
 
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(itemsFile);
        
        if (doc != null) {
            Element root = doc.getDocumentElement();            
            NodeList itemsList = root.getChildNodes();
            
            for (int i = 0; i < itemsList.getLength(); i++) {
                Node node1 = itemsList.item(i);

                if (node1 instanceof Element) {
                    Element item = (Element) node1;                    
                    NodeList itemChildren = item.getChildNodes();
                    String name = null;
                    BigDecimal cost = null;
                    
                    for (int j = 0; j < itemChildren.getLength(); j++) {
                        Node node2 = itemChildren.item(j);

                        if (node2 instanceof Element) {
                            Element itemChild = (Element) node2;

                            if (itemChild.getTagName().equals("name")) {
                                Text textNode = (Text) itemChild.getFirstChild();
                                name = textNode.getData().trim();
                            }

                            if (itemChild.getTagName().equals("cost")) {
                                Text textNode = (Text) itemChild.getFirstChild();
                                cost = new BigDecimal(textNode.getData().trim());
                            }
                        }
                    }      
                    items.put(name, new Item(name, cost));
                }
            }
        }
        
        return Collections.unmodifiableMap(items);  
    }
    /**
     *  Enter to infinity loop, where waiting for Client connections and processing them in separate thread
     */
    public void processMessagesFromClient() throws ParserConfigurationException, SAXException {
        try {
            openConnection();
            
            while(true) {
                // The accept() method blocks the application until a request is received, and then returns
                // the Socket (on an anonymous port) to communicate with the Client
                clientSocket = serverSocket.accept();
                
                DateTime startTime = new DateTime();
                System.out.println("Connection Time: " + startTime + " Client Info: " + clientSocket);
         
                // Load items
                Map<String, Item> items = loadItems();
                 
                // Thread for processing Client connection
                Thread t = new Thread(new ClientHandler(clientSocket, logedClients, items));
                t.start();   
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Launch the application
     * 
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        Server server = new Server();
        server.processMessagesFromClient();
    }
    
}
